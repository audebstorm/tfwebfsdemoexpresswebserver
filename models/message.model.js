const { DataTypes, Sequelize, ModelStatic } = require('sequelize');

/**
 * Constructeur du model Message
 * @param {Sequelize} sequelize
 * @returns { ModelStatic<any> }
 */
module.exports = (sequelize) => {

    //define (nomModel, attributsModel, optionsModel)
    const Message = sequelize.define('Message', {
        //id est autogénéré en PK (entier autoincrémenté, qui va démarrer à 1)
        author: {
            type : DataTypes.STRING,
            allowNull : false
        },
        title : {
            type : DataTypes.STRING,
            allowNull : false
        },
        msg : {
            type : DataTypes.STRING,
            allowNull : false,
        },
        urgent : {
            type : DataTypes.BOOLEAN,
            allowNull : false,
            defaultValue : false
        }
    }, {
        tableName : 'Message', //Pour indiquer le nom de la table qu'il doit créer à la sychronisation, sinon, pas défaut, il prend le nom du Model, avec un s à la fin
    })

    return Message;
}