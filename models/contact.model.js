const { Sequelize, ModelStatic, DataTypes } = require("sequelize");


/**
 * Modèle Contact
 * @param {Sequelize} sequelize
 * @return {ModelStatic<any>}
 */
module.exports = (sequelize) => {
    //Création du modèle define(nomModel, attributs, options)
    const Contact = sequelize.define('Contact', {
        firstname : {
            type : DataTypes.STRING,
            allowNull : false
        },
        lastname : {
            type : DataTypes.STRING,
            allowNull : false
        },
        email : {
            type : DataTypes.STRING,
            allowNull : false
        },
        tel : {
            type : DataTypes.STRING,
            allowNull : false
        },
        favorite : {
            type : DataTypes.BOOLEAN,
            allowNull : false,
            defaultValue : false
        }
    }, {
        tableName: 'Contact'
    })

    //On renvoie le modèle
    return Contact;
}