//Config variables d'environnement
require('dotenv').config();

//Import d'express
const express = require('express');

//Connection à la db et synchronisation
//Import de la db
const db = require('./models');
//Connection
db.sequelize.authenticate()
    .then(() => console.log('Connection to DB success'))
    .catch((err) => console.log('Connection to DB failed', err))
//Synchronisation (uniquement si on est en mode dev)
if(process.env.NODE_ENV === 'development') {
    //db.sequelize.sync( { force : true } ) //Créer les tables, si existent déjà, les supprime et les refait
    db.sequelize.sync( { alter : { drop : false} } );//Autorise la modification des tables/columns mais n'autorise pas la suppression de tables/columns
}

//Création du server
const app = express();

//Middleware application lvl
//Logger (Morgan)
const morgan = require('morgan');
app.use(morgan('tiny'));
//Gestion des données POST des formulaires
app.use(express.urlencoded({ extended : true }));

//Configuration du moteur de vues
app.set('view engine', 'ejs'); //Pour indiquer à l'app qu'elle doit utiliser ejs comme moteur de vue (view engine)
//Note : si vous voulez en utiliser un autre :
// app.set('view engine', 'pug') -> Ne pas oublier d'installer pug
app.set('views', 'views'); //Pour indiquer le dossier dans lequel les vues doivent être cherchées

//Gestion des dossiers statiques
//Pour rendrer accessible le dossier public et faire en sorte que les images, les fichiers css etc soient gérés
app.use(express.static('public'));

//Configuration de l'app, pour qu'elle utilise un router
const router = require('./router'); //import du router
app.use(router); //utilisation du router dans l'app


//Lancement du serveur
app.listen(process.env.PORT, () => {
    console.log(`Server started on port:${process.env.PORT}`);
});