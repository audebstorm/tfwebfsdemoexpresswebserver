const db = require("../models");

const contactController = {
    getContacts : async (req, res) => {
        try {
            const contacts = await db.Contact.findAll();
            const datas = { title : 'Contacts', contacts};
            res.render('contact/contact', datas);
        }
        catch(err) {
            res.sendStatus(500);
        }
        
    },
    getForm : (req, res) => {
        const data = { title : 'Ajouter contact'};
        res.render('contact/addContact', data);

    },
    postForm : async (req, res) => {
        try {
            const data = {
                ...req.body, //spread operator
                favorite : req.body.favorite === 'on' ? true : false
            };

            await db.Contact.create(data);

            res.redirect('/contact');
            
        }
        catch(err) {
            res.sendStatus(500);
        }
    }
}

module.exports = contactController;