const contactController = require('../controllers/contact.controller');
const homeController = require('../controllers/home.controller');
const messageController = require('../controllers/message.controller');

//Création du router grâce à express
const router = require('express').Router();

//Configuration des routes possibles :
//Lien vers la home page
router.get('/', homeController.getHome );
//Lien vers la liste de tous les messages
router.get('/message', messageController.getMessages);
//Lien vers le formulaire pour add un message
router.get('/message/ajouter', messageController.getForm)
//Post du formulaire
router.post('/message/ajouter', messageController.postMessage);

//Ajout des routes pour l'exo contact
router.get('/contact', contactController.getContacts);
router.get('/contact/ajouter', contactController.getForm);
router.post('/contact/ajouter', contactController.postForm);

//Export du router
module.exports = router;